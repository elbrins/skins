#!/bin/bash

opkg install --force-reinstall enigma2-plugin-skins-openvix-youvix-green >/dev/null 2>&1

opkg install --force-reinstall enigma2-plugin-skins-openvix-youvix-purple >/dev/null 2>&1

opkg install --force-reinstall enigma2-plugin-skins-openvix-youvix-blue >/dev/null 2>&1

opkg install --force-reinstall enigma2-plugin-skins-openvix-youvix-red >/dev/null 2>&1

#config
skin=youvix-xe-mod-hamdy
version=1.0
url=https://gitlab.com/eliesat/skins/-/raw/main/openvix/youvix-xe-mod-hamdy-1.0.tar.gz
package=/var/volatile/tmp/$skin-$version.tar.gz

echo "> Downloading $skin-$version skin  please wait ..."
sleep 3s

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf package >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then 
echo "> $skin-$version skin installed successfully"
echo "> Uploaded By ElieSat"
sleep 3s
else
echo "> $skin-$version skin installation failed"
sleep 3s
fi
fi

exit 0
