if [ -d /usr/share/enigma2/BLACK_DRAGON_FHD ]; then
echo "> removing package please wait..."
sleep 3s 

rm -rf /usr/lib/enigma2/python/Plugins/Extensions/TeamNitro >/dev/null 2>&1

rm -rf /usr/share/enigma2/BLACK_DRAGON_FHD >/dev/null 2>&1

rm -rf /usr/share/enigma2/GOLD_DRAGON_FHD >/dev/null 2>&1

rm -rf /usr/share/enigma2/RED_DRAGON_FHD >/dev/null 2>&1

rm -rf /usr/share/enigma2/SILVER_DRAGON_FHD >/dev/null 2>&1

rm -rf /usr/share/enigma2/TN_Skins_version >/dev/null 2>&1

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3s

else

#config
skin=dragon-fhd
version=2.0
package=/var/volatile/tmp/$skin-$version.tar.gz
url=https://gitlab.com/eliesat/skins/-/raw/main/all/teamnitro/dragon/dragon-fhd-2.0.tar.gz


echo "> Downloading $skin-$version skin  please wait ..."
sleep 3s

if [ ! -d /usr/lib/enigma2/python/Plugins/Extensions/TeamNitro ]; then
wget -q "--no-check-certificate" https://gitlab.com/eliesat/skins/-/raw/main/all/teamnitro/teamnitro.sh -O - | /bin/sh 
fi


#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf package >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then 
echo "> $skin-$version skin installed successfully"
echo "> Uploaded By ElieSat"
sleep 3s
else
echo "> $skin-$version skin installation failed"
sleep 3s
fi
fi
exit 0
