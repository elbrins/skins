if [ -d /usr/share/enigma2/Premium-FHD-Black ]; then
echo "> removing package please wait..."
sleep 3s 
rm -rf /usr/share/enigma2/Premium-FHD-Black >/dev/null 2>&1

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3s

else

#config
skin=premium-fhd-black
version=2.7
url=https://gitlab.com/eliesat/skins/-/raw/main/all/premium-fhd/premium-fhd-black-2.7.tar.gz
package=/var/volatile/tmp/$skin-$version.tar.gz

echo "> Downloading $skin-$version skin  please wait ..."
sleep 3s

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf $package >/dev/null 2>&1

SKINDIR='/usr/share/enigma2/Premium-FHD-Black'
TMPDIR='/tmp'


if grep -qs -i "openATV" /etc/image-version; then
mv $SKINDIR/image_logo/openatv/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/oaweather_plugin.tar.gz $TMPDIR
mv -f $SKINDIR/pfiles/AtileHD.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf oaweather_plugin.tar.gz
tar -xzf AtileHD.tar.gz
cp -r usr /
cd ..   
   
elif grep -qs -i "egami" /etc/image-version; then
mv $SKINDIR/image_logo/egami/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/oaweather_plugin.tar.gz $TMPDIR
mv -f $SKINDIR/pfiles/AtileHD.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf oaweather_plugin.tar.gz
tar -xzf AtileHD.tar.gz
cp -r usr /
cd ..
	
elif grep -qs -i "openbh" /etc/image-version; then
mv $SKINDIR/image_logo/obh/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/obh/skin.xml $SKINDIR
mv -f $SKINDIR/pfiles/obh/skin_templates.xml $SKINDIR
mv -f $SKINDIR/pfiles/weather_plugin.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf weather_plugin.tar.gz
cp -r usr /
cd ..
	
elif grep -qs -i "openvix" /etc/image-version; then
mv $SKINDIR/image_logo/openvix/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/obh/skin.xml $SKINDIR
mv -f $SKINDIR/pfiles/obh/skin_templates.xml $SKINDIR
	echo "Installing oldweather plugin..."
mv -f $SKINDIR/pfiles/weather_plugin.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf weather_plugin.tar.gz
cp -r usr /
cd ..
	
elif grep -qs -i "PURE2" /etc/image-version; then
mv $SKINDIR/image_logo/pure2/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/oaweather_plugin.tar.gz $TMPDIR
mv -f $SKINDIR/pfiles/AtileHD.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf oaweather_plugin.tar.gz
tar -xzf AtileHD.tar.gz
cp -r usr /
cd ..
	
elif [ -r /usr/lib/enigma2/python/OPENDROID ]; then
mv $SKINDIR/image_logo/opendroid/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/obh/skin.xml $SKINDIR
mv -f $SKINDIR/pfiles/obh/skin_templates.xml $SKINDIR
mv -f $SKINDIR/pfiles/weather_plugin.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf weather_plugin.tar.gz
cp -r usr /
cd ..
	
elif grep -qs -i "OpenSPA" /etc/image-version; then
mv $SKINDIR/image_logo/openspa/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/openspa/skin.xml $SKINDIR
mv -f $SKINDIR/pfiles/openspa/skin_templates.xml $SKINDIR
mv -f $SKINDIR/pfiles/oaweather_plugin.tar.gz $TMPDIR
mv -f $SKINDIR/pfiles/AtileHD.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf oaweather_plugin.tar.gz
tar -xzf AtileHD.tar.gz
cp -r usr /
cd ..
	
elif grep -qs -i "teamBlue" /etc/image-version; then
mv $SKINDIR/image_logo/teamblue/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/openpli/skin.xml $SKINDIR
mv -f $SKINDIR/pfiles/openpli/skin_templates.xml $SKINDIR
mv -f $SKINDIR/pfiles/weather_plugin.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf weather_plugin.tar.gz
cp -r usr /
cd ..
	
elif grep -qs -i "openpli" /etc/issue; then
mv $SKINDIR/image_logo/openpli/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/openpli/skin.xml $SKINDIR
mv -f $SKINDIR/pfiles/openpli/skin_templates.xml $SKINDIR
mv -f $SKINDIR/pfiles/weather_plugin.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf weather_plugin.tar.gz
cp -r usr /
cd ..

elif grep -qs -i "areadeltasat" /etc/issue; then
mv $SKINDIR/image_logo/openpli/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/openpli/skin.xml $SKINDIR
mv -f $SKINDIR/pfiles/openpli/skin_templates.xml $SKINDIR
mv -f $SKINDIR/pfiles/weather_plugin.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf weather_plugin.tar.gz
cp -r usr /
cd ..
	
elif grep -qs -i "satlodge" /etc/issue; then
mv $SKINDIR/image_logo/satlodge/imagelogo.png $SKINDIR
mv -f $SKINDIR/pfiles/openpli/skin.xml $SKINDIR
mv -f $SKINDIR/pfiles/openpli/skin_templates.xml $SKINDIR
mv -f $SKINDIR/pfiles/weather_plugin.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf weather_plugin.tar.gz
cp -r usr /
cd ..
	
else
echo ""
fi
sleep 2

rm -rf $SKINDIR/pfiles  > /dev/null 2>&1
rm -rf $SKINDIR/image_logo  > /dev/null 2>&1
rm -rf /control  > /dev/null 2>&1
rm -rf $TMPDIR/weather_plugin.tar.gz > /dev/null 2>&1
rm -rf $TMPDIR/oaweather_plugin.tar.gz > /dev/null 2>&1
rm -rf $TMPDIR/AtileHD.tar.gz > /dev/null 2>&1
rm -rf $TMPDIR/usr > /dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then 
echo "> $skin-$version skin installed successfully"
echo "> Uploaded By ElieSat"
sleep 3s
else
echo "> $skin-$version skin installation failed"
sleep 3s
fi
fi

exit 0
