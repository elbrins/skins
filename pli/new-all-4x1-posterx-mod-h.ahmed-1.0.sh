if [ -d /usr/share/enigma2/New-Black_PosterX ]; then
echo "> removing package please wait..."
sleep 3s 
rm -rf /usr/share/enigma2/New-Black_PosterX >/dev/null 2>&1

rm -rf /usr/share/enigma2/New-Blue_PosterX >/dev/null 2>&1

rm -rf /usr/share/enigma2/New-Violet_PosterX >/dev/null 2>&1

rm -rf /usr/share/enigma2/New-transparent_PosterX >/dev/null 2>&1

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3s

# Check python
pyVersion=$(python -c"from sys import version_info; print(version_info[0])")

if [ "$pyVersion" -ne 2 ]; then
echo "> install py2 image & try again..."

exit 0

else

#config
skin=new-all-4x1-posterx-mod-h.ahmed
version=1.0
url=https://gitlab.com/eliesat/skins/-/raw/main/pli/new-all-4x1-posterx-mod-h.ahmed-1.0.tar.gz
package=/var/volatile/tmp/$skin-$version.tar.gz

echo "> Downloading $skin-$version skin  please wait ..."
sleep 3s

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf package >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then 
echo "> $skin-$version skin installed successfully"
echo "> Uploaded By ElieSat"
sleep 3s
else
echo "> $skin-$version skin installation failed"
sleep 3s
fi
fi
fi
exit 
