#!/bin/sh

#script: wget -q "--no-check-certificate" https://gitlab.com/eliesat/skins/-/raw/main/obh/obh-mx-livefhd-2.0.sh -O - | /bin/sh

#config
package=mx-livefhd
version=2.0


#remove old skin#
rm -rf /usr/share/enigma2/MX-LiveFHD
if [  -d "/control" ]; then
rm -r  /control
fi
rm -rf /control
rm -rf /postinst
rm -rf /preinst
rm -rf /prerm
rm -rf /postrm
rm -rf /tmp/*.ipk
rm -rf /tmp/*.tar.gz

#download package

echo "> Downloading "$package" "$version" Skin  Please Wait ..."
sleep 10s

wget -O /var/volatile/tmp/"$package"-"$version".tar.gz --no-check-certificate "https://gitlab.com/eliesat/skins/-/raw/main/obh/obh-mx-livefhd-2.0.tar.gz"

echo "> Installing "$package" "$version" Skin  Please Wait ..."
sleep 10s


#extract new skin#
tar -xf /var/volatile/tmp/"$package"-"$version".tar.gz -C /
MY_RESULT=$?

#remove files from tmp#
rm -f /var/volatile/tmp/"$package"-"$version".tar.gz > /dev/null 2>&1

echo ''
if [ $MY_RESULT -eq 0 ]; then 
echo "> "$package" "$version" Skin Installed Successfully"
echo "> Uploaded by ElieSat"

else
echo "> "$package"-"$version" Skin Installation Failed"
fi
exit 0


